# Clase 4

Materiales para la Cuarta clase, en esta clase cubrimos:

- Repaso de Programación Orientada a Objetos y Tests unitarios
- Patrones de Diseño, implementación de Singleton y Strategy
- Soporte al modelo de clases del TPI

## Lista de Materiales

- [Apunte 05 - Patrones de Diseño](./patrones/README.md)
- [Apunte 06 - Tests Unitarios](./testing/README.md)

## Lista de Ejemplos

- ver ejemplos de implementación de Patrones en proyecto Ejemplos Clase
- [Introducción a JUnit](./testing/introJunit/)

## Estado general de la semana

Versión para publicación 2023.

***

## Software requerido

- Java
- Maven
- IntelliJ idea

## Clonar el presente repositorio

``` bash
cd existing_repo
git remote add origin https://gitlab.com/felisteffo.dev/arg-programa/clase-4/lecturas-4.git
git branch -M main
git push -uf origin main
```

## Autores

Felipe Steffolani - basado en un material original de Federico Bett y German Romani para la Cátedra de Backend de Aplicaciones

## License

Este trabajo está licenciado bajo una Licencia Creative Commons Atribución-NoComercial-CompartirIgual 4.0 Internacional. Para ver una copia de esta licencia, visita [https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es](!https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es).
