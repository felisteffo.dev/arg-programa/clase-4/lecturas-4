# Apunte 5 - Patrones de Diseño

En el desarrollo de software actual, para la creación de aplicaciones empresariales hay demasiados lenguajes de programación distintos para distintas capas, arquitecturas, y otras preocupaciones que pueden hacer difícil decidir por dónde empezar. Ahí es donde los patrones de diseño entran.

Un patrón de diseño es como una plantilla para tu proyecto o solución a un problema. Usa ciertas convenciones y puedes esperar un comportamiento de tipo específico. Estos patrones fueron inventados según las diversas experiencias de los desarrolladores, así que son realmente como distintos conjuntos de mejores prácticas.

El trabajo es decidir cuál conjunto de mejores prácticas es la más útil para el problema o proyecto que enfrentamos. Según el patrón de diseño que se elija, todos empezarán a tener expectativas de lo que el código debería estar haciendo y qué vocabulario todos estarán usando.

Los Patrones de Diseño de Programación pueden ser usados en todos los lenguajes de programación y pueden ser usados para coincidir en cualquier proyecto porque brindan solamente un esquema general de una solución a un problema específico.

Hay 23 patrones oficiales del libro _Design Patterns - Elements of Reusable Object-Oriented Software_, el cual es considerado uno de los libros más influyentes en la teoría de orientación a objetos y desarrollo de software.

Aquí vamos a revisar cuatro de esos patrones de diseño para darte una idea de lo que son algunos de los patrones y cuando podemos usarlo.

## El patrón de diseño singleton

El patrón singleton solamente permite garantiza que de una clase exista una y solo una instancia independientemente de cómo o desde dónde se comunique el resto de la aplicación con esta clase. Usa un atributo de clase para almacenar esa instancia e impide que de la misma se creen instancias directamente a partir de `new`. Al no poder crear la instancia directamente va a hacer falta quién provea la instancia si vamos a plantear ese proveedor con un método de clase que se encarga de crear la instancia si no existe y devolver la instancia que mencionamos.

Eso previene que múltiples instancias de esta clase existan al mismo tiempo en la memoria de la VM, lo cual podría causar errores raros, por ejemplo: si tenemos una clase que posee la configuración de la aplicación en memoria y tenemos más de una instancia, y en una de ellas realizamos una mutación de un valor de configuración, podríamos tener otras instancias que siguen teniendo el valor anterior.

Un ejemplo de un singleton la clase que brinda acceso a las variables de configuración de una aplicación.

```java
package ar.edu.utnfc.argprog.singleton;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

public final class Configuracion implements Iterable<String> {

    // Miembros de Clase ==> para el Singletón
    // Atributo del Singleton
    private static Configuracion instancia = null;

   // Método de
    public static Configuracion getInstancia() {

       if (instancia == null) {

           instancia = new Configuracion();
       }
       return instancia;
    }

    // Miembros de instancia
    // Atributos de la instancia
    private Properties p;

    // Constructor de la instancia
    private Configuracion() {

        try (InputStream inStream = getClass().getClassLoader().getResourceAsStream("application.properties")) {
            p = new Properties();
            p.load(inStream);

        }
        catch (IOException e) {
            System.err.println("Fatal: no existe el archivo de configuración");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public String getValue(String clave) {

        return p.getProperty(clave);
    }

    public void setValue(String clave, String valor) {

        p.setProperty(clave, valor);
    }

    public Iterator<String> iterator() {
        return p.stringPropertyNames().iterator();
    }


}
```

La clase Configuración plantea el acceso a la colección Properties de propiedades de configuración. En esta clase podemos observar el constructor privado, la instancia como atributo de clase y el método de clase getInsncia que se encarga de crear la instancia por primera vez si no existe y a partir de allí devolver siempre esta misma instancia.

```java
package ar.edu.utnfc.argprog;

import ar.edu.utnfc.argprog.singleton.Configuracion;

public class App 
{
    public static void main( String[] args )
    {
        System.out.println("Propiedades del archivo de configuración");
        for(String propName : Configuracion.getInstancia()) {
            System.out.println(propName + " ==> " + Configuracion.getInstancia().getValue(propName));
        }

    }
}


```

## El patrón de diseño estrategia

El patrón estrategia es como una versión avanzada de una sentencia if else. Intenta resolver el problema que se plantea cuando para cierto método existen distintas alternativas de implementación que pueden ser utilizadas de forma independiente a requerimiento del consumidor del objeto.

Se plantea una interfaz para un método que tienes en tu clase base. Esta interfaz es usada luego para encontrar la implementación correcta de ese método que debería ser implementado en una clase derivada. La implementación, en este caso, será decidida en el tiempo de ejecución basado en el cliente.

En vez de que el cliente busque una implementación, lo delega a la interfaz de estrategia y la estrategia encuentra la implementación correcta. Un uso común para esto son los sistemas de procesamiento de pago.

Podrías tener un carrito de compras que solamente permita a los clientes pagar con sus tarjetas de crédito, pero perderás clientes que quieran usar otros métodos de pago.

El patrón de diseño estrategia nos permite desacoplar los métodos de pago desde el proceso de pago, lo cual significa que podemos agregar o actualizar estrategias sin cambiar ningún código en el carrito de compras o en el proceso de pago.

Entre los ejemplos de clase dejamos planteado un ejemplo que plantea diferentes estrategias de pago para una Orden de compra.

![picture 0](../images/38eb0d7344659fc1cd8580add1adad14c2cd3f66af45daa56ec52adb6f3c1a2a.png)  

Con el patrón estrategia, también puedes cambiar dinámicamente la estrategia que es usada en tiempo de ejecución. Eso significa que serás capaz de cambiar la estrategia, o la implementación del método, que está siendo usado según la entrada de usuario o el entorno donde la app está corriendo.

Cuando sea que el cliente comience a ir a través del proceso de pago en tu sitio web, el método de pago por defecto que encuentren será la implementación de PayPal el cual viene del constructor. Esto podría fácilmente ser actualizado si el cliente selecciona un método de pago diferente.

El patrón de diseño estrategia es poderoso cuando lidias con métodos que tienen múltiples implementaciones. Podría sentirse como que estás usando una interfaz, pero no tienes que escribir una implementación para el método cada vez que lo llames en una clase distinta. Te da más flexibilidad que las interfaces.
